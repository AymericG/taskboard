$(document).ready(function(){
	
	var vm = new CardBoard([
		{ Color: "green", Top: 100, Left: 50, Text: "Sample Card 1" },
		{ Color: "blue", Top: 200, Left: 50, Text: "Sample Card 2" }
	]);
	
	ko.applyBindings(vm, $(".card-board-window")[0]);
});