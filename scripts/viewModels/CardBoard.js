var CardBoard = function(cards){
	var self = this;

	var cardTypes = ["", "blue", "green", "amethyst", "asphalt", "turquoise", "carrot"];

    this.Mapping = {
        'Cards': {
            create: function (options) { 
                return new Card(options.data);
            }
        }
    };
	ko.mapping.fromJS({ Cards: cards }, this.Mapping, this);

	$(window).on("wp:delete-card", function(e1, e2, ui){
		var card = ko.dataFor($(ui.helper)[0]);
		self.Cards.remove(card);
	});

	$(window).on("wp:drag-card", function(e1, e2, ui){
		var card = ko.dataFor($(ui.helper)[0]);
		card.Top(ui.position.top);
		card.Left(ui.position.left);
	});

	this.AddCard = function(viewModel, e) {
		var originalEvent = e.originalEvent;
		var sumWidth = 0;
		$(".card").each(function() { sumWidth += $(this).outerWidth(); });

		var x = originalEvent.pageX - e.currentTarget.offsetLeft - sumWidth - 30;
		var y = originalEvent.pageY - e.currentTarget.offsetTop - 30;

		var randomColor = cardTypes[Math.floor(Math.random() * cardTypes.length)];
		self.Cards.push(new Card({Text: "", Color: randomColor, Left: x, Top: y}));
	};
};