var Card = function(data){
	var self = this;

	ko.mapping.fromJS(data, {}, this);

	this.TopPx = ko.computed(function(){
		return self.Top() + "px";
	});

	this.LeftPx = ko.computed(function(){
		return self.Left() + "px";
	});

	this.Style = ko.computed(function(){
		return "top: " + self.TopPx() + "; left: " + self.LeftPx() + "; position: relative;";
	});

	this.CssClass = ko.computed(function(){
		return self.Color() + " card";
	});

};