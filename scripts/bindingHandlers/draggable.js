ko.bindingHandlers.draggable = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        $(element).draggable({ 
        	containment: "parent",
        	cancel: ".editor", 
        	
        	drag: function(event, ui)
        	{
                $(window).trigger(valueAccessor(), [event, ui]);
        	}

        });
    }
};