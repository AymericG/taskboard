ko.bindingHandlers.droppable = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        $(element).droppable({
            hoverClass: "drophover",
            tolerance: 'pointer',
            drop: function (event, ui) {
                $(window).trigger(valueAccessor(), [event, ui]);
            }
        });
    }
};